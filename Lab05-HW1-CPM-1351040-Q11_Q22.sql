
--Thai Thien 1351040
USE   HW01_ProjectManagement_1351040 
go


--11. Retrieve the employee whose address is in �Houston, Texas�.
SELECT e.*
FROM EMPLOYEE as e
WHERE e.Address Like '%Houston, TX%'

--12. Find the employees who were born during the 1950s.
SELECT *
FROM EMPLOYEE
WHERE BDate > '1-Jan-1950' AND BDate < '31-Dec-1959'

--13. For each employees who are working for the �Product Xbox� project, retrieve the salaries (after increasing 10 percent).
SELECT e.FName,e.Minit,e.LName,e.Salary+Salary*0.1 as Salary
FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w
WHERE p.PName = 'Product Xbox'
		AND P.PNumber=W.PNO
		AND w.ESSN = e.SSN

--14. Retrieve the department 5�s employee whose salary is between $30,000 and $40,000.
SELECT *
FROM EMPLOYEE as e
WHERE e.Salary >= 30000
	AND e.Salary <=40000
	AND e.DNO = 5

--15. Retrieve the name and home address of all employees working for the �Research� department.
SELECT concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName, e.Address
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE d.DName = 'Research'
	AND d.DNumber = e.DNO

--16. Retrieve the full name of the employee whose dependents have the same first name and same sex to that employee.
SELECT  concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName
FROM EMPLOYEE as e, DEPENDENT as d
WHERE e.SSN = d.ESSN
	AND e.Sex = d.Sex
	AND e.FName = d.Dependent_Name

--17. Show the name of all departments in which have more than 2 employees. (Using SQL joins).
SELECT d.DName,d.DNumber,d.MgrSSN,d.MgrStartDate
FROM DEPARTMENT as d JOIN EMPLOYEE as e ON d.DNumber = e.DNO
WHERE d.DNumber = e.DNO
GROUP BY d.DName,d.DNumber,d.MgrSSN,d.MgrStartDate
HAVING COUNT(*)>2

--18. Calculate the number of employees in the company and the number of employees in the �Research� department.
SELECT COUNT (distinct e1.FName) as TotalEmployee, (SELECT COUNT(distinct e2.FName) 
 									FROM EMPLOYEE as e2, DEPARTMENT as d
									WHERE e2.DNO = d.DNumber
										 AND d.DName = 'Research')as ResearchEmployee
FROM EMPLOYEE as e1, DEPARTMENT as d
WHERE e1.DNO = d.DNumber

--19. Calculate the number of distinct salaries in Company.
SELECT COUNT(distinct e.Salary)
FROM EMPLOYEE as e

--20. For each employees, list the employee�s full-name and calculate the number of dependents (of that employee) who have the same sex with that employee.
SELECT e.*,(SELECT COUNT(d.Dependent_Name)
			FROM DEPENDENT as d
			WHERE d.ESSN = e.SSN
				AND
				  d.Sex = e.Sex
			)
FROM EMPLOYEE as e

	



-- Max employee (subquery for 21)
SELECT MAX(t.ENum)
FROM (SELECT COUNT(e.FName) as ENum
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.DNO = d.DNumber
GROUP BY d.DNumber) as t

--21. Retrieve all departments which have maximum number of employees. Show the name of department, the number of employees who are working in that department.
SELECT d.DName, COUNT(e.FName) as NumOfEmmployee
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.DNO = d.DNumber
GROUP BY d.DNumber,d.DName
HAVING COUNT(e.FName) = (SELECT MAX(t.ENum)
					FROM (SELECT COUNT(e.FName) as ENum
					FROM EMPLOYEE as e, DEPARTMENT as d
					WHERE e.DNO = d.DNumber
					GROUP BY d.DNumber) as t)


--22. Calculate the total salary of employees who work in �Research� department, as well as the maximum salary, the minimum salary, and the average salary in that department.
SELECT MAX(e.Salary) as Maximum, MIN(e.Salary) as Minimum, AVG(e.Salary) as Average
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.DNO = d.DNumber
	AND d.DName ='Research'
