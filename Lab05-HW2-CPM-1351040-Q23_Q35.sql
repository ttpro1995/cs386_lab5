
--Thai Thien 1351040
USE   HW01_ProjectManagement_1351040 
go

--23. Retrieve the first name, last name and the year of birth of each employees.
SELECT e.FName, e.LName, YEAR(e.BDate) as YearOfBirth
FROM EMPLOYEE as e


--24. Retrieve the first name, last name and the age of each employees.
SELECT e.FName, e.LName, (YEAR(GETDATE()) - YEAR(e.BDate)) as age
FROM EMPLOYEE as e


--25. Retrieve the employee (FName, LName) and his/her working projects (PNumber, PName, and Name of the department which manages this project); the query�s results are ordered by department name first and then ordered alphabetically by last name, first name in the same department.
SELECT e.FName, e.LName, p.PNumber, p.PName, d.DName
FROM EMPLOYEE as e, PROJECT as p, DEPARTMENT as d, WORKS_ON as w
WHERE e.SSN = w.ESSN
    AND w.PNO = p.PNumber
	AND p.DNum = d.DNumber
ORDER BY d.DName,e.LName,e.FName

--26. Retrieve the employees who were born in between 1955 and 1960.
-- query 5
SELECT *
FROM EMPLOYEE
WHERE BDate > '1-Jan-1955' AND BDate < '31-Dec-1960'

--27. Retrieve the name and home address of all employees working for the �Research� department.
SELECT concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName, e.Address
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE d.DName = 'Research'
	AND d.DNumber = e.DNO

--28. For each employee, retrieve the employee�s first name, last name and the first and last name of his immediate supervisor.
SELECT emp.FName as 'employee�s first name ', emp.LName as 'employee�s last name', sup.FName as 'supervisor�s first name', sup.LName as 'supervisor�s last name'
FROM EMPLOYEE AS emp, EMPLOYEE AS sup
WHERE emp.SupperSSN=sup.SSN

--29. Retrieve the number of projects that involve an employee whose last name is �Smith�, (who is either as a worker or as a manager of the department which controls that project).
SELECT distinct p.PNumber
FROM EMPLOYEE as e, PROJECT as p, WORKS_ON as w, DEPARTMENT as d
WHERE
	(  d.MgrSSN = e.SSN
		AND
		e.LName Like '%Smith%'
	)
	OR
	(
		w.PNO = p.PNumber
		AND w.ESSN = e.SSN
		AND  e.LName Like '%Smith%'
	)

--30. List the names of managers who have at least one dependent.
SELECT e.FName, e.LName 
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.SSN = d.MgrSSN
    AND EXISTS(
	SELECT *
	FROM DEPENDENT as d
	WHERE d.ESSN = e.SSN
)

--31. Retrieve the names of all employees who works in department 5 and work more than 10 hours per week on the �Product iPhone7� project.
SELECT distinct concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName
FROM EMPLOYEE as e, DEPARTMENT as d, WORKS_ON as w, PROJECT as p
WHERE e.DNO = 5
     AND w.Hours >10
	 AND p.PName ='Product iPhone7'
	 AND w.ESSN = e.SSN
	 AND w.PNO = p.PNumber
	 



--32. For each projects, list the project name and the total hours per week (by all employees) spent on that project.
SELECT p.PName, (SELECT sum(w.Hours) 
				FROM WORKS_ON as w
 				WHERE w.PNO = p.PNumber
)
FROM   PROJECT as p


--33. Retrieve all employees whose salary is greater than the average salary of �Research� department.
--average salary of research
SELECT AVG(e.Salary)
FROM EMPLOYEE as e, DEPARTMENT as d
WHERE e.DNO = d.DNumber
	AND d.DName ='Research'

----Retrieve all employees whose salary is greater than the average salary of �Research� department.
SELECT e.*
FROM EMPLOYEE as e
WHERE e.Salary > (SELECT AVG(e.Salary)
					FROM EMPLOYEE as e, DEPARTMENT as d
					WHERE e.DNO = d.DNumber
							AND d.DName ='Research')



--34. List all name of employees that works on all projects controlled by �Research� department.
SELECT distinct concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName
FROM EMPLOYEE as e, DEPARTMENT as d, PROJECT as p, WORKS_ON as w
WHERE d.DName ='Research'
	AND p.DNum = d.DNumber
	AND w.PNO = p.PNumber
	and w.ESSN = e.SSN

--35. For each employee that works on all projects controlled by Research department, retrieve the name, address and the number of dependents.
SELECT distinct concat(e.FName,' ',e.Minit,'. ',e.LName) as FullName,e.Address,(SELECT COUNT(d.Dependent_Name) 
																				FROM DEPENDENT as d
																				WHERE d.ESSN = e.SSN
																				) as 'number of dependents'  
FROM EMPLOYEE as e
WHERE NOT EXISTS ((SELECT p.PNumber
					FROM PROJECT as p,DEPARTMENT as d
					WHERE p.DNum = d.DNumber
					AND d.DName ='Research'
					-- all research project
					) EXCEPT (SELECT w.PNO
								FROM WORKS_ON as w
								WHERE w.ESSN =e.SSN 
								--all project that employee work
								))


	-------35 new

SELECT  e.FName, e.Address, COUNT(d.Dependent_Name) as 'number of dependents'
FROM EMPLOYEE as e, DEPENDENT as d
WHERE NOT EXISTS ((SELECT p.PNumber
					FROM PROJECT as p,DEPARTMENT as d
					WHERE p.DNum = d.DNumber
					AND d.DName ='Research'
					-- all research project
					) EXCEPT (SELECT w.PNO
								FROM WORKS_ON as w
								WHERE w.ESSN =e.SSN 
								--all project that employee work
								))
	AND e.SSN = d.ESSN
GROUP BY e.FName,e.Address,e.SSN, d.ESSN



	-------



